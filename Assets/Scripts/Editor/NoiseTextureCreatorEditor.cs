﻿using UnityEditor;

namespace Editor
{
	[CustomEditor(typeof(NoiseTextureCreator))]
	public class NoiseTextureCreatorEditor : UnityEditor.Editor
	{

		private NoiseTextureCreator m_noiseTextureCreator;
		#region PublicMethods

		private void OnEnable()
		{
			m_noiseTextureCreator = target as NoiseTextureCreator;
			Undo.undoRedoPerformed += RefreshCreator;
		}

		private void OnDisable()
		{
			Undo.undoRedoPerformed -= RefreshCreator;
		}

		private void RefreshCreator()
		{
			m_noiseTextureCreator.FillTexture();
		}

		public override void OnInspectorGUI()
		{
			EditorGUI.BeginChangeCheck();
			DrawDefaultInspector();

			if (EditorGUI.EndChangeCheck())
			{
				RefreshCreator();
			}
		}

		#endregion
	}
}
