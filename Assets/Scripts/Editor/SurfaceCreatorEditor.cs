﻿using UnityEditor;

namespace Editor
{
	[CustomEditor(typeof(SurfaceCreator))]
	public class SurfaceCreatorEditor : UnityEditor.Editor
	{

		private SurfaceCreator m_surfaceCreator;
		#region PublicMethods

		private void OnEnable()
		{
			 m_surfaceCreator = target as SurfaceCreator;
			Undo.undoRedoPerformed += RefreshCreator;
		}

		private void OnDisable()
		{
			Undo.undoRedoPerformed -= RefreshCreator;
		}

		private void RefreshCreator()
		{
			m_surfaceCreator.Refresh();
		}

		public override void OnInspectorGUI()
		{
			EditorGUI.BeginChangeCheck();
			DrawDefaultInspector();

			if (EditorGUI.EndChangeCheck())
			{
				RefreshCreator();
			}
		}

		#endregion
	}
}
