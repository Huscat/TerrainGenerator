﻿using UnityEngine;

public delegate float NoiseMethod(Vector3 point, float frequency);

public enum NoiseMethodType
{
	HardValue,
	Value,
	Perlin
}

public static class Noise
{
	#region PublicFields

	private static float m_sqr2 = Mathf.Sqrt(2f);

	public static NoiseMethod[] HardValueMethods =
	{
		HardValue1D,
		HardValue2D,
		HardValue3D
	};

	public static NoiseMethod[] ValueMethods =
	{
		Value1D,
		Value2D,
		Value3D
	};

    public static NoiseMethod[] PerlinMethods =
	{
		Perlin1D,
		Perlin2D,
		Perlin3D
	};

	public static NoiseMethod[][] Methods =
	{
		HardValueMethods,
		ValueMethods,
		PerlinMethods
	};

	#endregion

	#region PrivateFields

	private const int HASH_MASK = 255;
	private const int GRADIENTS_MASK_1_D = 1;
	private const int GRADIENTS_MASK_2_D = 7;
	private const int GRADIENTS_MASK_3_D = 15;

	private static int[] m_hash =
	{
		151, 160, 137, 91, 90, 15, 131, 13, 201, 95, 96, 53, 194, 233, 7, 225,
		140, 36, 103, 30, 69, 142, 8, 99, 37, 240, 21, 10, 23, 190, 6, 148,
		247, 120, 234, 75, 0, 26, 197, 62, 94, 252, 219, 203, 117, 35, 11, 32,
		57, 177, 33, 88, 237, 149, 56, 87, 174, 20, 125, 136, 171, 168, 68, 175,
		74, 165, 71, 134, 139, 48, 27, 166, 77, 146, 158, 231, 83, 111, 229, 122,
		60, 211, 133, 230, 220, 105, 92, 41, 55, 46, 245, 40, 244, 102, 143, 54,
		65, 25, 63, 161, 1, 216, 80, 73, 209, 76, 132, 187, 208, 89, 18, 169,
		200, 196, 135, 130, 116, 188, 159, 86, 164, 100, 109, 198, 173, 186, 3, 64,
		52, 217, 226, 250, 124, 123, 5, 202, 38, 147, 118, 126, 255, 82, 85, 212,
		207, 206, 59, 227, 47, 16, 58, 17, 182, 189, 28, 42, 223, 183, 170, 213,
		119, 248, 152, 2, 44, 154, 163, 70, 221, 153, 101, 155, 167, 43, 172, 9,
		129, 22, 39, 253, 19, 98, 108, 110, 79, 113, 224, 232, 178, 185, 112, 104,
		218, 246, 97, 228, 251, 34, 242, 193, 238, 210, 144, 12, 191, 179, 162, 241,
		81, 51, 145, 235, 249, 14, 239, 107, 49, 192, 214, 31, 181, 199, 106, 157,
		184, 84, 204, 176, 115, 121, 50, 45, 127, 4, 150, 254, 138, 236, 205, 93,
		222, 114, 67, 29, 24, 72, 243, 141, 128, 195, 78, 66, 215, 61, 156, 180,

		151, 160, 137, 91, 90, 15, 131, 13, 201, 95, 96, 53, 194, 233, 7, 225,
		140, 36, 103, 30, 69, 142, 8, 99, 37, 240, 21, 10, 23, 190, 6, 148,
		247, 120, 234, 75, 0, 26, 197, 62, 94, 252, 219, 203, 117, 35, 11, 32,
		57, 177, 33, 88, 237, 149, 56, 87, 174, 20, 125, 136, 171, 168, 68, 175,
		74, 165, 71, 134, 139, 48, 27, 166, 77, 146, 158, 231, 83, 111, 229, 122,
		60, 211, 133, 230, 220, 105, 92, 41, 55, 46, 245, 40, 244, 102, 143, 54,
		65, 25, 63, 161, 1, 216, 80, 73, 209, 76, 132, 187, 208, 89, 18, 169,
		200, 196, 135, 130, 116, 188, 159, 86, 164, 100, 109, 198, 173, 186, 3, 64,
		52, 217, 226, 250, 124, 123, 5, 202, 38, 147, 118, 126, 255, 82, 85, 212,
		207, 206, 59, 227, 47, 16, 58, 17, 182, 189, 28, 42, 223, 183, 170, 213,
		119, 248, 152, 2, 44, 154, 163, 70, 221, 153, 101, 155, 167, 43, 172, 9,
		129, 22, 39, 253, 19, 98, 108, 110, 79, 113, 224, 232, 178, 185, 112, 104,
		218, 246, 97, 228, 251, 34, 242, 193, 238, 210, 144, 12, 191, 179, 162, 241,
		81, 51, 145, 235, 249, 14, 239, 107, 49, 192, 214, 31, 181, 199, 106, 157,
		184, 84, 204, 176, 115, 121, 50, 45, 127, 4, 150, 254, 138, 236, 205, 93,
		222, 114, 67, 29, 24, 72, 243, 141, 128, 195, 78, 66, 215, 61, 156, 180
	};

	private static float[] m_gradients1D =
	{
		-1f, 1f
	};

	private static Vector2[] m_gradients2D =
	{
		new Vector2(+1f, 0f),
		new Vector2(-1f, 0f),
		new Vector2(0f, +1f),
		new Vector2(0f, -1f),

		new Vector2(+1f, +1f).normalized,
		new Vector2(-1f, +1f).normalized,
		new Vector2(+1f, -1f).normalized,
		new Vector2(-1f, -1f).normalized
	};

	private static Vector3[] m_gradients3D =
	{
		new Vector3(+1f, +1f, 0f),
		new Vector3(-1f, +1f, 0f),
		new Vector3(+1f, -1f, 0f),
		new Vector3(-1f, -1f, 0f),
		new Vector3(+1f, 0f, +1f),
		new Vector3(-1f, 0f, +1f),
		new Vector3(+1f, 0f, -1f),
		new Vector3(-1f, 0f, -1f),
		new Vector3(0f, +1f, +1f),
		new Vector3(0f, -1f, +1f),
		new Vector3(0f, +1f, -1f),
		new Vector3(0f, -1f, -1f),

		new Vector3(+1f, +1f, 0f),
		new Vector3(-1f, +1f, 0f),
		new Vector3(0f, -1f, +1f),
		new Vector3(0f, -1f, -1f)
	};

	#endregion

	#region PublicMethods

	public static float SumNoise(NoiseMethod method, Vector3 point, float frequency, int octaves, float lacunarity, float persistance)
	{
		float sum = method(point, frequency);
		float amplitude = 1;
		float range = 1;
		for (int i = 1; i < octaves; i++)
		{
			frequency *= lacunarity;
			amplitude *= persistance;
			range += amplitude;
			sum += method(point, frequency) * amplitude;
		}

		return sum / range;
	}

	public static float Perlin1D(Vector3 point, float frequency)
	{
		point *= frequency;

		int i0 = Mathf.FloorToInt(point.x);
		float t0 = point.x - i0;
		float t1 = t0 - 1;
		i0 &= HASH_MASK;
		int i1 = i0 + 1;

		float g0 = m_gradients1D[m_hash[i0] & GRADIENTS_MASK_1_D];
		float g1 = m_gradients1D[m_hash[i1] & GRADIENTS_MASK_1_D];

		float v0 = g0 * t0;
		float v1 = g1 * t1;

		float t = Smooth(t0);

		return Mathf.Lerp(v0, v1, t) * 2f;
	}

	public static float Perlin2D(Vector3 point, float frequency)
	{
		point *= frequency;

		int ix0 = Mathf.FloorToInt(point.x);
		int iy0 = Mathf.FloorToInt(point.y);
		float tx0 = point.x - ix0;
		float ty0 = point.y - iy0;
		float tx1 = tx0 - 1;
		float ty1 = ty0 - 1;
		ix0 &= HASH_MASK;
		iy0 &= HASH_MASK;
		int ix1 = ix0 + 1;
		int iy1 = iy0 + 1;

		int h0 = m_hash[ix0];
		int h1 = m_hash[ix1];
		Vector2 g00 = m_gradients2D[m_hash[h0 + iy0] & GRADIENTS_MASK_2_D];
		Vector2 g10 = m_gradients2D[m_hash[h1 + iy0] & GRADIENTS_MASK_2_D];
		Vector2 g01 = m_gradients2D[m_hash[h0 + iy1] & GRADIENTS_MASK_2_D];
		Vector2 g11 = m_gradients2D[m_hash[h1 + iy1] & GRADIENTS_MASK_2_D];

		float v00 = Dot(g00, tx0, ty0);
		float v10 = Dot(g10, tx1, ty0);
		float v01 = Dot(g01, tx0, ty1);
		float v11 = Dot(g11, tx1, ty1);

		float tx = Smooth(tx0);
		float ty = Smooth(ty0);

		float v0 = Mathf.Lerp(v00, v10, tx);
		float v1 = Mathf.Lerp(v01, v11, tx);

		return Mathf.Lerp(v0, v1, ty) * m_sqr2;
	}

	public static float Perlin3D(Vector3 point, float frequency)
	{
		point *= frequency;

		int ix0 = Mathf.FloorToInt(point.x);
		int iy0 = Mathf.FloorToInt(point.y);
		int iz0 = Mathf.FloorToInt(point.z);
		float tx0 = point.x - ix0;
		float ty0 = point.y - iy0;
		float tz0 = point.z - iz0;
		float tx1 = tx0 - 1;
		float ty1 = ty0 - 1;
		float tz1 = tz0 - 1;
		ix0 &= HASH_MASK;
		iy0 &= HASH_MASK;
		iz0 &= HASH_MASK;
		int ix1 = ix0 + 1;
		int iy1 = iy0 + 1;
		int iz1 = iz0 + 1;

		int h0 = m_hash[ix0];
		int h1 = m_hash[ix1];
		int h00 = m_hash[h0 + iy0];
		int h10 = m_hash[h1 + iy0];
		int h01 = m_hash[h0 + iy1];
		int h11 = m_hash[h1 + iy1];
		Vector3 g000 = m_gradients3D[m_hash[h00 + iz0] & GRADIENTS_MASK_3_D];
		Vector3 g100 = m_gradients3D[m_hash[h10 + iz0] & GRADIENTS_MASK_3_D];
		Vector3 g010 = m_gradients3D[m_hash[h01 + iz0] & GRADIENTS_MASK_3_D];
		Vector3 g110 = m_gradients3D[m_hash[h11 + iz0] & GRADIENTS_MASK_3_D];
		Vector3 g001 = m_gradients3D[m_hash[h00 + iz1] & GRADIENTS_MASK_3_D];
		Vector3 g101 = m_gradients3D[m_hash[h10 + iz1] & GRADIENTS_MASK_3_D];
		Vector3 g011 = m_gradients3D[m_hash[h01 + iz1] & GRADIENTS_MASK_3_D];
		Vector3 g111 = m_gradients3D[m_hash[h11 + iz1] & GRADIENTS_MASK_3_D];

		float v000 = Dot(g000, tx0, ty0, tz0);
		float v100 = Dot(g100, tx1, ty0, tz0);
		float v010 = Dot(g010, tx0, ty1, tz0);
		float v110 = Dot(g110, tx1, ty1, tz0);
		float v001 = Dot(g001, tx0, ty0, tz1);
		float v101 = Dot(g101, tx1, ty0, tz1);
		float v011 = Dot(g011, tx0, ty1, tz1);
		float v111 = Dot(g111, tx1, ty1, tz1);

		float tx = Smooth(tx0);
		float ty = Smooth(ty0);
		float tz = Smooth(tz0);

		float i00 = Mathf.Lerp(v000, v100, tx);
		float i10 = Mathf.Lerp(v010, v110, tx);
		float i01 = Mathf.Lerp(v001, v101, tx);
		float i11 = Mathf.Lerp(v011, v111, tx);

		float i0 = Mathf.Lerp(i00, i10, ty);
		float i1 = Mathf.Lerp(i01, i11, ty);

		return Mathf.Lerp(i0, i1, tz);
	}

	public static float Value1D(Vector3 point, float frequency)
	{
		point *= frequency;

		int i0 = Mathf.FloorToInt(point.x);
		float t = point.x - i0;
		i0 &= HASH_MASK;
		int i1 = i0 + 1;

		int h0 = m_hash[i0];
		int h1 = m_hash[i1];

		t = Smooth(t);
		float i = Mathf.Lerp(h0, h1, t);

		return i * (1f / HASH_MASK);
	}

	public static float Value2D(Vector3 point, float frequency)
	{
		point *= frequency;

		int ix0 = Mathf.FloorToInt(point.x);
		int iy0 = Mathf.FloorToInt(point.y);
		float tx = point.x - ix0;
		float ty = point.y - iy0;
		ix0 &= HASH_MASK;
		iy0 &= HASH_MASK;
		int ix1 = ix0 + 1;
		int iy1 = iy0 + 1;

		int h0 = m_hash[ix0];
		int h1 = m_hash[ix1];
		int h00 = m_hash[h0 + iy0];
		int h10 = m_hash[h1 + iy0];
		int h01 = m_hash[h0 + iy1];
		int h11 = m_hash[h1 + iy1];

		tx = Smooth(tx);
		ty = Smooth(ty);

		float i0 = Mathf.Lerp(h00, h10, tx);
		float i1 = Mathf.Lerp(h01, h11, tx);

		return Mathf.Lerp(i0, i1, ty) * (1f / HASH_MASK);
	}

	public static float Value3D(Vector3 point, float frequency)
	{
		point *= frequency;

		int ix0 = Mathf.FloorToInt(point.x);
		int iy0 = Mathf.FloorToInt(point.y);
		int iz0 = Mathf.FloorToInt(point.z);
		float tx = point.x - ix0;
		float ty = point.y - iy0;
		float tz = point.z - iz0;
		ix0 &= HASH_MASK;
		iy0 &= HASH_MASK;
		iz0 &= HASH_MASK;
		int ix1 = ix0 + 1;
		int iy1 = iy0 + 1;
		int iz1 = iz0 + 1;

		int h0 = m_hash[ix0];
		int h1 = m_hash[ix1];
		int h00 = m_hash[h0 + iy0];
		int h10 = m_hash[h1 + iy0];
		int h01 = m_hash[h0 + iy1];
		int h11 = m_hash[h1 + iy1];
		int h000 = m_hash[h00 + iz0];
		int h100 = m_hash[h10 + iz0];
		int h010 = m_hash[h01 + iz0];
		int h110 = m_hash[h11 + iz0];
		int h001 = m_hash[h00 + iz1];
		int h101 = m_hash[h10 + iz1];
		int h011 = m_hash[h01 + iz1];
		int h111 = m_hash[h11 + iz1];

		tx = Smooth(tx);
		ty = Smooth(ty);
		tz = Smooth(tz);

		float i00 = Mathf.Lerp(h000, h100, tx);
		float i10 = Mathf.Lerp(h010, h110, tx);
		float i01 = Mathf.Lerp(h001, h101, tx);
		float i11 = Mathf.Lerp(h011, h111, tx);

		float i0 = Mathf.Lerp(i00, i10, ty);
		float i1 = Mathf.Lerp(i01, i11, ty);

		return Mathf.Lerp(i0, i1, tz) * (1f / HASH_MASK);
	}

	public static float HardValue1D(Vector3 point, float frequency)
	{
		point *= frequency;

		int i = Mathf.FloorToInt(point.x) & HASH_MASK;

		return m_hash[i] * (1f / HASH_MASK);
	}

	public static float HardValue2D(Vector3 point, float frequency)
	{
		point *= frequency;

		int ix = Mathf.FloorToInt(point.x) & HASH_MASK;
		int iy = Mathf.FloorToInt(point.y) & HASH_MASK;

		return m_hash[m_hash[ix] + iy] * (1f / HASH_MASK);
	}

	public static float HardValue3D(Vector3 point, float frequency)
	{
		point *= frequency;

		int ix = Mathf.FloorToInt(point.x) & HASH_MASK;
		int iy = Mathf.FloorToInt(point.y) & HASH_MASK;
		int iz = Mathf.FloorToInt(point.z) & HASH_MASK;

		return m_hash[m_hash[(m_hash[ix] + iy) & HASH_MASK] + iz] * (1f / HASH_MASK);
	}

	#endregion

	#region PrivateMethods

	private static float Smooth(float x)
	{
		return x * x * x * (x * (x * 6f - 15f) + 10f);
	}

	private static float Dot(Vector2 gradient, float x, float y)
	{
		return gradient.x * x + gradient.y * y;
	}

	private static float Dot(Vector3 gradient, float x, float y, float z)
	{
		return gradient.x * x + gradient.y * y + gradient.z * z;
	}

	#endregion
}
