﻿using UnityEngine;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class SurfaceCreator : MonoBehaviour
{
	#region SerializeFields

	[SerializeField] private NoiseMethodType m_type;
	[SerializeField] [Range(1, 3)] private int m_dimensions = 3;
	[SerializeField] [Range(1, 200)] private int m_resolution = 10;
	[SerializeField] private float m_frequency = 4;
	[SerializeField] [Range(0f, 1f)] private float m_strength = 1;
	[SerializeField] [Range(1, 8)] private int m_octaves = 1;
	[SerializeField] [Range(1f, 4f)] private float m_lacunarity = 2f;
	[SerializeField] [Range(0f, 1f)] private float m_persistence = 0.5f;
	[SerializeField] private Gradient m_coloring;
	[SerializeField] private Vector3 m_positionOffset;
	[SerializeField] private Vector3 m_rotationOffset;
	[SerializeField] private bool m_damping;

	[SerializeField] private bool m_showNormals;

	#endregion

	#region PrivateFields

	private Mesh m_surfaceMesh;
	private int m_currentResolution;

	private Vector3[] m_vertices;
	private Color[] m_colors;
	private Vector3[] m_normals;

	#endregion

	#region UnityMethods

	private void OnEnable()
	{
		if (m_surfaceMesh == null)
		{
			m_surfaceMesh = new Mesh { name = "Surface" };
			GetComponent<MeshFilter>().mesh = m_surfaceMesh;
		}

		Refresh();
	}

	#endregion

	#region PublicMethods

	public void Refresh()
	{
		if (m_resolution != m_currentResolution)
		{
			CreateGrid();
		}

		Quaternion rotationOffset = Quaternion.Euler(m_rotationOffset);
		Vector3 point00 = rotationOffset * new Vector3(-0.5f, -0.5f) + m_positionOffset;
		Vector3 point10 = rotationOffset * new Vector3(+0.5f, -0.5f) + m_positionOffset;
		Vector3 point01 = rotationOffset * new Vector3(-0.5f, +0.5f) + m_positionOffset;
		Vector3 point11 = rotationOffset * new Vector3(+0.5f, +0.5f) + m_positionOffset;

		NoiseMethod noiseMethod = Noise.Methods[(int)m_type][m_dimensions - 1];
		float amplitude = m_damping ? m_strength / m_frequency : m_strength;
		float gridSize = 1f / m_resolution;
		for (int v = 0, y = 0; y <= m_resolution; y++)
		{
			Vector3 point0 = Vector3.Lerp(point00, point01, y * gridSize);
			Vector3 point1 = Vector3.Lerp(point10, point11, y * gridSize);
			for (int x = 0; x <= m_resolution; x++, v++)
			{
				Vector3 facePoint = Vector3.Lerp(point0, point1, (x + 0.5f) * gridSize);
				float shadeSample = Noise.SumNoise(noiseMethod, facePoint, m_frequency, m_octaves, m_lacunarity, m_persistence);
				shadeSample = m_type == NoiseMethodType.Perlin ? shadeSample * 0.5f : shadeSample - 0.5f;
				shadeSample *= amplitude;

				m_vertices[v].y = shadeSample;
				m_colors[v] = m_coloring.Evaluate(shadeSample + 0.5f);
			}
		}

		m_surfaceMesh.vertices = m_vertices;
		m_surfaceMesh.colors = m_colors;
		m_surfaceMesh.RecalculateNormals();
		m_normals = m_surfaceMesh.normals;
	}

	#endregion

	#region PrivateMethods

	private void CreateGrid()
	{
		m_currentResolution = m_resolution;
		float squareSize = 1f / m_resolution;
		m_surfaceMesh.Clear();

		m_vertices = new Vector3[(m_resolution + 1) * (m_resolution + 1)];
		m_colors = new Color[m_vertices.Length];
		m_normals = new Vector3[m_vertices.Length];
		var uv = new Vector2[m_vertices.Length];
		for (int v = 0, z = 0; z <= m_resolution; z++)
		{
			for (int x = 0; x <= m_resolution; x++, v++)
			{
				m_vertices[v] = new Vector3(x * squareSize - 0.5f, 0f, z * squareSize - 0.5f);
				m_colors[v] = Color.black;
				m_normals[v] = Vector3.up;
				uv[v] = new Vector2(x * squareSize, z * squareSize);
			}
		}

		m_surfaceMesh.vertices = m_vertices;
		m_surfaceMesh.colors = m_colors;
		m_surfaceMesh.normals = m_normals;
		m_surfaceMesh.uv = uv;

		var triangles = new int[6 * m_resolution * m_resolution];
		for (int t = 0, v = 0, y = 0; y < m_resolution; y++, v++)
		{
			for (int x = 0; x < m_resolution; x++, t += 6, v++)
			{
				triangles[t] = v;
				triangles[t + 1] = v + m_resolution + 1;
				triangles[t + 2] = v + 1;

				triangles[t + 3] = v + 1;
				triangles[t + 4] = v + m_resolution + 1;
				triangles[t + 5] = v + m_resolution + 2;
			}
		}

		m_surfaceMesh.triangles = triangles;
	}

	#endregion

	private void OnDrawGizmosSelected()
	{
		if (m_showNormals && m_vertices != null)
		{
			float gridSize = 1f / m_resolution;
			Gizmos.color = Color.yellow;
			for (int v = 0; v < m_vertices.Length; v++)
			{
				Gizmos.DrawRay(m_vertices[v], m_normals[v] * gridSize);
			}
		}
	}
}
