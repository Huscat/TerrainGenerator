﻿using UnityEngine;

// [ExecuteInEditMode]
public class NoiseTextureCreator : MonoBehaviour
{
	#region SerializeFields

	[SerializeField] [Range(2, 512)] private int m_resolution = 256;
	[SerializeField] private NoiseMethodType m_type;
	[SerializeField] private float m_frequency = 32;
	[SerializeField] [Range(1, 3)] private int m_dimensions = 3;
	[SerializeField] [Range(1, 8)] private int m_octaves = 1;
	[SerializeField] [Range(1, 4)] private float m_lacunarity = 2f;
	[SerializeField] [Range(0, 1)] private float m_persistence = 0.5f;
	[SerializeField] private Gradient m_colorLevels;


    #endregion

    #region PrivateFields

    private Texture2D m_texture;

	#endregion

	#region UnityMethods

	private void OnEnable()
	{
		if (m_texture == null)
		{
			m_texture = new Texture2D(m_resolution, m_resolution, TextureFormat.RGB24, true)
			{
				name = "Procedural",
				wrapMode = TextureWrapMode.Clamp,
				filterMode = FilterMode.Trilinear,
				anisoLevel = 9
			};

			GetComponent<MeshRenderer>().sharedMaterial.mainTexture = m_texture;
		}

		FillTexture();
	}

	private void Update()
	{
		if (transform.hasChanged)
		{
			transform.hasChanged = false;
			FillTexture();
		}
	}

	#endregion

	#region PublicMethods

	public void FillTexture()
	{
		if (m_texture.height != m_resolution)
		{
			m_texture.Resize(m_resolution, m_resolution);
		}

		var vertexPoints = new Vector3[2, 2];
		vertexPoints[0, 0] = transform.TransformPoint(new Vector3(-0.5f, -0.5f));
		vertexPoints[1, 0] = transform.TransformPoint(new Vector3(+0.5f, -0.5f));
		vertexPoints[0, 1] = transform.TransformPoint(new Vector3(-0.5f, +0.5f));
		vertexPoints[1, 1] = transform.TransformPoint(new Vector3(+0.5f, +0.5f));

		NoiseMethod noiseMethod = Noise.Methods[(int)m_type][m_dimensions - 1];
		float pixelSize = 1f / m_resolution;
		for (int y = 0; y < m_resolution; y++)
		{
			var edgePoints = new Vector3[2];
			edgePoints[0] = Vector3.Lerp(vertexPoints[0, 0], vertexPoints[0, 1], (y + 0.5f) * pixelSize);
			edgePoints[1] = Vector3.Lerp(vertexPoints[1, 0], vertexPoints[1, 1], (y + 0.5f) * pixelSize);
			for (int x = 0; x < m_resolution; x++)
			{
				Vector3 facePoint = Vector3.Lerp(edgePoints[0], edgePoints[1], (x + 0.5f) * pixelSize);
				float shadeSample = Noise.SumNoise(noiseMethod, facePoint, m_frequency, m_octaves, m_lacunarity, m_persistence);
				if (m_type == NoiseMethodType.Perlin)
				{
					shadeSample = shadeSample * 0.5f + 0.5f;
				}

                m_texture.SetPixel(x, y, m_colorLevels.Evaluate(shadeSample));
			}
		}

		m_texture.Apply();
	}

	#endregion
}
